import requests
import json

from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_location_photo(city, state):
    try:
        url = "https://api.pexels.com/v1/search"
        params = {"query": city + " " + state, "per_page": 1}
        headers = {"Authorization": PEXELS_API_KEY}

        res = requests.get(url, params=params, headers=headers)
        unencoded = json.loads(res.content)

        url = unencoded["photos"][0]["url"]

        return {"picture_url": url}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data(city, state):
    geo_base_url = "http://api.openweathermap.org/geo/1.0/direct?"
    complete_base_url = (
        geo_base_url
        + city
        + ","
        + state
        + "&limit=1&appid="
        + OPEN_WEATHER_API_KEY
    )
    res = requests.get(complete_base_url)

    try:
        lat = json.loads(res.content)[0]["lat"]
        lon = json.loads(res.content)[0]["lon"]
    except (KeyError, IndexError):
        return {"weather": None}

    weather_base_url = "https://api.openweathermap.org/data/2.5/weather?lat="
    weather_complete_url = (
        weather_base_url
        + str(lat)
        + "&lon="
        + str(lon)
        + "&appid="
        + OPEN_WEATHER_API_KEY
        + "&units=imperial"
    )
    response = requests.get(weather_complete_url)

    try:
        temp = json.loads(response.content)["main"]["temp"]
        description = json.loads(response.content)["weather"][0]["description"]
        weather = {"temp": temp, "description": description}
        return weather
    except (KeyError, IndexError):
        return {"weather": None}
